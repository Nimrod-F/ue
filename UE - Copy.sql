﻿USE Master
IF EXISTS(SELECT * FROM sys.databases WHERE NAME='UE')
	DROP DATABASE UE
CREATE DATABASE UE
GO

USE UE
CREATE TABLE Data_infiintare(
	ID_D TINYINT PRIMARY KEY IDENTITY(1,1),
	data INT,
	locul NVARCHAR(50)
	
);
GO

CREATE TABLE Anul_aderarii(
	ID_A TINYINT PRIMARY KEY IDENTITY(1,1),
	Data DATE,
	Anul_TratatuluiAderarii INT
);
GO
CREATE TABLE Stat(
	ID_S SMALLINT PRIMARY KEY IDENTITY(1,1),
	Nume NVARCHAR(50),
	Populatie BIGINT,
	Moneda NVARCHAR(20),
	ID_AnAderare TINYINT FOREIGN KEY REFERENCES Anul_aderarii
);
GO
CREATE TABLE Locatie(
	ID_L TINYINT PRIMARY KEY IDENTITY(1,1),
	Nume NVARCHAR(50),
	ID_Stat SMALLINT FOREIGN KEY REFERENCES Stat 
);
GO

CREATE TABLE Institutii(
	ID_I TINYINT PRIMARY KEY IDENTITY(1,1),
	Nume NVARCHAR(50) UNIQUE,
	Nr_membrii SMALLINT,
	Presedinte NVARCHAR(50),
	Pag_web NVARCHAR(50),
	ID_Data TINYINT FOREIGN KEY REFERENCES Data_infiintare,
	ID_Locatie TINYINT FOREIGN KEY REFERENCES Locatie

);
GO


CREATE TABLE Membrii(
	ID_M SMALLINT PRIMARY KEY IDENTITY(1,1),
	Nume NVARCHAR(50),
	Prenume NVARCHAR(50),
	Varsta TINYINT,
	ID_Institutie TINYINT FOREIGN KEY REFERENCES Institutii,
	ID_State SMALLINT FOREIGN KEY REFERENCES Stat 
);
GO


CREATE TABLE Rol(
	ID_R TINYINT PRIMARY KEY IDENTITY(1,1),
	Dscriere NVARCHAR(200),
	Importanta TINYINT,
	CHECK (Importanta>=1 AND Importanta<=5)
);
GO

CREATE TABLE Activitati(
	ID_Institutie TINYINT FOREIGN KEY REFERENCES Institutii,
	ID_Rol TINYINT FOREIGN KEY REFERENCES Rol
);
GO

CREATE TABLE Limbi_oficiale(
	ID_L SMALLINT PRIMARY KEY IDENTITY(1,1),
	Nume NVARCHAR(50),
	Familia_lingvistica NVARCHAR(50)
);
GO

CREATE TABLE Vorbire(
	ID_Limba SMALLINT FOREIGN KEY REFERENCES Limbi_oficiale,
	ID_Stat SMALLINT FOREIGN KEY REFERENCES Stat,
	Procentaj SMALLINT,
	CHECK (Procentaj>=0 AND Procentaj<=100)
);
GO


INSERT INTO Data_infiintare Values(1952, 'Bruxelles')
INSERT INTO Data_infiintare Values(1974, 'Paris')
INSERT INTO Data_infiintare Values(1958, 'Berlin')
INSERT INTO Data_infiintare Values(1958, 'Madrid')
INSERT INTO Data_infiintare Values(1952, 'Bruxelles')
INSERT INTO Data_infiintare Values(1998, 'Paris')
INSERT INTO Data_infiintare Values(1977, 'Bruxelles')

INSERT INTO Anul_aderarii Values('19580101', null)
INSERT INTO Stat VALUES('Luxemburg', 562958, 'Euro',1)
INSERT INTO Anul_aderarii Values('19580101', null)
INSERT INTO Stat VALUES('Belgia', 11000000, 'Euro',2)
INSERT INTO Anul_aderarii Values('19580101', null)
INSERT INTO Stat VALUES('Franta',  66415161, 'Euro', 3)
INSERT INTO Anul_aderarii Values('20040501', '2002')
INSERT INTO Stat VALUES('Ungaria', 9855571, 'HUF', 4)
INSERT INTO Anul_aderarii Values('20130701', '2012')
INSERT INTO Stat VALUES('Croatia', 4225316, 'kuna', 5)
INSERT INTO Anul_aderarii Values('19580101', null)
INSERT INTO Stat VALUES('Germania', 65853652, 'Euro', 6)
INSERT INTO Anul_aderarii VALUES('20070101', 2010)
INSERT INTO Stat VALUES('Romania', 18000000, 'RON', 7)

INSERT INTO Locatie VALUES('Luxembourg', 1)
INSERT INTO Locatie VALUES('Bruxelles', 2)
INSERT INTO Locatie VALUES('Strausbourg', 3)
INSERT INTO Locatie VALUES('Frankfurt', 6)

INSERT INTO Institutii VALUES('Parlamentul European', 751, 'Martin Schulz', 'www.eu-parlament_europ.com',1,2)
--INSERT INTO Institutii VALUES('Parlamentul European', 751, 'Martin Schulz', 'www.eu-parlament_europ.com',1,2)
INSERT INTO Institutii VALUES('Consiliul European', 27, 'Donald Tusk', 'www.eu-european_consil.com',2,1)
INSERT INTO Institutii VALUES('Consiliul Uniunii Europeane', 27, 'Viktor Orban', 'www.eu-european_consil_union.com',3,2)
INSERT INTO Institutii VALUES('Comisia Europeana', 27, 'Jean-Claude Juncker', 'www.eu-comisia.com',4,2)
INSERT INTO Institutii VALUES('Curtea de Justiție a Uniunii Europene', 73, null, 'www.eu-CTA.com',5,1)
INSERT INTO Institutii VALUES('Banca Centrală Europeană', 48, null, 'www.BCE', 6, 4)
INSERT INTO Institutii VALUES('Curtea Europeană de Conturi', 27,'Vítor Manuel da Silva Caldeira', 'www.CEC', 7, 1)

INSERT INTO Membrii VALUES('Becali', 'Gigi', 45, 1, 4)
INSERT INTO Membrii VALUES('Orban', 'Victor', 40, 2, 4)
INSERT INTO Membrii VALUES('Betty', 'Tokes', 35, 7, 2)
INSERT INTO Membrii VALUES('Ion', 'Ghita', 55, 2, 3)
INSERT INTO Membrii VALUES('Ana', 'Maria', 65, 5, 6)
INSERT INTO Membrii VALUES('Liviu', 'Marin', 68, 5, 6)
INSERT INTO Membrii VALUES('Gizi', 'Ion', 58, 4, 4)
INSERT INTO Membrii VALUES('Hege', 'Hans', 46, 3, 1)

INSERT INTO Rol VALUES('prioritatiole politice', 3)
INSERT INTO Rol VALUES('tratarea chestiunilor complexe', 4)
INSERT INTO Rol VALUES('adoptarea de legi', 5)
INSERT INTO Rol VALUES('judeca', 5)
INSERT INTO Rol VALUES('finanteaza', 3)
INSERT INTO Rol VALUES('supravegheaza celalte institutii', 4)
INSERT INTO Rol VALUES('blocheaza si prevede coruperea', 3)
INSERT INTO Rol VALUES('gestioneaza economia blocului unitar', 2)

INSERT INTO Activitati VALUES(1, 3)
INSERT INTO Activitati VALUES(1, 2)
INSERT INTO Activitati VALUES(2, 1)
INSERT INTO Activitati VALUES(5, 3)
INSERT INTO Activitati VALUES(3, 4)
INSERT INTO Activitati VALUES(1, 6)
INSERT INTO Activitati VALUES(2, 6)
INSERT INTO Activitati VALUES(5, 7)
INSERT INTO Activitati VALUES(6, 8)
INSERT INTO Activitati VALUES(7, 8)

UPDATE Stat SET Moneda='Euro' WHERE Moneda<>'Euro'
SELECT * FROM Stat

UPDATE Institutii SET Presedinte='Anghela MErker' WHERE Presedinte='Martin Schulz'
SELECT * FROM Institutii

UPDATE Data_infiintare SET locul='Bruxell' WHERE data BETWEEN 1952 AND 1977
SELECT * FROM Data_infiintare

UPDATE Anul_aderarii SET Anul_TratatuluiAderarii=1952 WHERE Anul_TratatuluiAderarii IS NULL
SELECT * FROM Anul_aderarii

UPDATE Membrii SET Nume=null WHERE Varsta>60
SELECT * FROM Membrii

--DELETE FROM Institutii WHERE ID_Locatie IN (SELECT L.ID_L
	--										FROM Locatie L
		---									WHERE L.Nume LIKE '_%bourg')
--Select * FROM Locatie

SELECT * FROM Stat

DELETE FROM Stat WHERE Moneda IN ('kuna', 'RON')
Select * FROM Stat

DELETE FROM Membrii WHERE Prenume LIKE ('I_%')
Select * FROM Membrii

Delete FROM Membrii WHERE Varsta>60
Select * FROM Membrii

