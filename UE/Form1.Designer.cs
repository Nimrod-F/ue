﻿namespace UE
{
    partial class State
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.stateGridView = new System.Windows.Forms.DataGridView();
            this.anul_aderariiGridView = new System.Windows.Forms.DataGridView();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.stateGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.anul_aderariiGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // stateGridView
            // 
            this.stateGridView.BackgroundColor = System.Drawing.Color.White;
            this.stateGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.stateGridView.GridColor = System.Drawing.SystemColors.Highlight;
            this.stateGridView.Location = new System.Drawing.Point(12, 188);
            this.stateGridView.Name = "stateGridView";
            this.stateGridView.Size = new System.Drawing.Size(377, 146);
            this.stateGridView.TabIndex = 0;
            // 
            // anul_aderariiGridView
            // 
            this.anul_aderariiGridView.BackgroundColor = System.Drawing.Color.White;
            this.anul_aderariiGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.anul_aderariiGridView.GridColor = System.Drawing.SystemColors.Highlight;
            this.anul_aderariiGridView.Location = new System.Drawing.Point(12, 12);
            this.anul_aderariiGridView.Name = "anul_aderariiGridView";
            this.anul_aderariiGridView.Size = new System.Drawing.Size(377, 159);
            this.anul_aderariiGridView.TabIndex = 1;
            this.anul_aderariiGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.anul_aderariiGridView_CellClick);
            this.anul_aderariiGridView.KeyUp += new System.Windows.Forms.KeyEventHandler(this.anul_aderariiGridView_KeyUp);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(411, 311);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(75, 23);
            this.buttonUpdate.TabIndex = 2;
            this.buttonUpdate.Text = "Actualizare";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.buttonUpdate_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(492, 311);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonAdd.TabIndex = 3;
            this.buttonAdd.Text = "Add";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(573, 311);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(75, 23);
            this.Delete.TabIndex = 11;
            this.Delete.Text = "Delete";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(395, 130);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(265, 175);
            this.panel1.TabIndex = 12;
            // 
            // State
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 380);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonUpdate);
            this.Controls.Add(this.anul_aderariiGridView);
            this.Controls.Add(this.stateGridView);
            this.Name = "State";
            this.Text = "State";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.State_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.stateGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.anul_aderariiGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView stateGridView;
        private System.Windows.Forms.DataGridView anul_aderariiGridView;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Panel panel1;
    }
}

