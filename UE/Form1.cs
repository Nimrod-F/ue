﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
namespace UE
{
    public partial class State : Form
    {
        static String con = ConfigurationManager.ConnectionStrings["cs"].ConnectionString;
        SqlConnection conn = new SqlConnection(con);

        //cream conexiunea la baza de date 
        //SqlConnection conn = new SqlConnection(@"Data Source = mobil\sqlexpress;Database=facturare; User ID=sa;Password="); SqlConnection conn = new SqlConnection(@"Data Source = mobil\sqlexpress; Initial Catalog=Facturare; Integrated Security=true");
        //SqlConnection conn = new SqlConnection("Data Source = NIMROD\\SQLEXPRESS; Initial Catalog = UE; Integrated Security = true");
        //creem DataSet-ul aici ca sa existe pe toata durata existentei form-ului
        List<TextBox> textBoxList;
        int idy = 0;
        DataSet ds = new DataSet();
        DataSet ds2 = new DataSet();
        SqlCommandBuilder cmdb;
        SqlDataAdapter anul_aderariiAdaptor=new SqlDataAdapter();
        SqlDataAdapter statAdaptor=new SqlDataAdapter();

        /*
        string ChildTableName = ConfigurationManager.AppSettings["FiuTable"];
        string ChildColumnNames = ConfigurationManager.AppSettings["ChildColumnNames"];
        string ColumnNamesInsertParameters = ConfigurationManager.AppSettings["ColumnNamesInsertParameters"];
        List<string> ColumnNamesList = new List<string>(ConfigurationManager.AppSettings["ColumnNames"].Split(','));
        SqlCommand cmd = new SqlCommand("INSERT INTO " + ChildTableName + " (" + ChildColumnNames + ") VALUES (" + ColumnNamesInsertParameters + ")", conn);
        foreach (string column in ColumnNamesList) { TextBox textBox = (TextBox)panel1.Controls[column]; cmd.Parameters.AddWithValue("@" + column, textBox.Text); }
        cs.Open(); 
        cmd.ExecuteNonQuery(); 
        ds.Clear(); 
        da.Fill(ds); 
        cs.Close();
        */

       
        public State()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            { //deschidem conexiunea conn.Open(); } catch (Exception ex) { MessageBox.Show(ex.Message); }
                //conn.Open();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }

            //Cream Adaptorul folosindu-ne de un Command (ascuns) care executa selectul, si de conexiunea deschisa mai devreme SqlDataAdapter facturiAdaptor = new SqlDataAdapter("select * from Facturi",conn);
            string select = ConfigurationSettings.AppSettings["SelectParinte"];
            anul_aderariiAdaptor.SelectCommand = new SqlCommand(select, conn);
            ds.Clear();
            //Adaptorul umplde DataSet-ul cu datele preluate din tabela Anul_aderarii 
            //in acelasi timp in DataSet este creata o tabela pe care o numim 'anul_aderarii' anul_aderariiAdaptor.Fill(ds,"anul_aderarii");
            string numeParinte = ConfigurationManager.AppSettings["ParinteTable"];
            anul_aderariiAdaptor.Fill(ds, numeParinte);
            //Legam Grid-ul, prin intermediul proprietatii DataSource, la tabela 'anul_aderarii' din DataSet 
            this.anul_aderariiGridView.DataSource = ds.Tables[numeParinte];//ds.Tables[0]
            //Apelam o functie creata pentru a popula al doilea grid 
            anul_aderariiGridView.AutoGenerateColumns = true;
            PopulateGridState(0);
            

        }

       
        private void PopulateGridState(int nr_rand)
        {
            //declaram o variabila pentru id-ul anul_aderarii selectata in primul grid 
            int idf;
            //extragem id-ul anului_aderarii din tabela facturi care e in DataSet 
            //puteam sa ne folosim si direct de datele din Grid 
            string numeParinte = ConfigurationManager.AppSettings["ParinteTable"];
            string id_Parinte = ConfigurationManager.AppSettings["Id_Parinte"];
            int.TryParse(ds.Tables[numeParinte].Rows[nr_rand][id_Parinte].ToString(), out idf);
            //creem un Adaptor al carui select il cream direct aici 
            //statAdaptor = new SqlDataAdapter("select * from Stat where c = " + idf.ToString(), conn);
            string selectFiu = ConfigurationSettings.AppSettings["SelectFiu"];
            statAdaptor.SelectCommand = new SqlCommand(selectFiu + idf.ToString(), conn);
            ds2.Clear();
            //cream in al doilea DataSet tabela detalii in care inseram datele din selectul de mai sus 
            string numeFiu = ConfigurationManager.AppSettings["FiuTable"];
            statAdaptor.Fill(ds2, numeFiu);

            //legam al doilea Grid la tabela detalii din al doilea DatSet 
            stateGridView.DataSource = ds2.Tables[numeFiu];
            stateGridView.AutoResizeColumns();
            stateGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            ds2.Dispose(); //inchidem DataSet-ul
            setUpTextBoxAndLabels();
        }

        private void anul_aderariiGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            PopulateGridState(e.RowIndex);
        }
        private void anul_aderariiGridView_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                case Keys.Down:
                case Keys.PageUp:
                case Keys.PageDown:
                    //MessageBox.Show(facturiGridView.CurrentRow.Index.ToString()); 
                    PopulateGridState(anul_aderariiGridView.CurrentRow.Index);
                    break;
                default:
                    break;
            }
        }

        private void State_FormClosing(object sender, FormClosingEventArgs e)
        {
            //conn.Close();
        }

        private void setUpTextBoxAndLabels()
        {
            idy = 0;
            textBoxList = new List<TextBox>();

            foreach (Control item in panel1.Controls.OfType<TextBox>())
            {
                panel1.Controls.Remove(item);
            }
            foreach (Control item in panel1.Controls.OfType<Label>())
            {
                panel1.Controls.Remove(item);
            }
            string numeFiu = ConfigurationManager.AppSettings["FiuTable"];
            int columnNr = ds2.Tables[numeFiu].Columns.Count;
           
            for (int i = 1; i < columnNr - 1; i++)
            {
               
                Point textP = new Point(120, idy * 44);
                TextBox textBox = new TextBox();
                textBox.Text = ds2.Tables[numeFiu].Columns[i].ColumnName;
                textBox.Location = textP;
                textBoxList.Add(textBox);
                idy++;
                panel1.Controls.Add(textBox);
            }

        }


        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string numeFiu = ConfigurationManager.AppSettings["FiuTable"];
                cmdb = new SqlCommandBuilder(this.statAdaptor);
                statAdaptor.Update(ds2, numeFiu);
                MessageBox.Show("Informatie Actualizata", "Update", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            int idA;
            int i;
            string value;
            int.TryParse(this.anul_aderariiGridView.CurrentRow.Cells[0].Value.ToString(), out idA);
            try
            {
                conn.Open();
                string insertFiu = ConfigurationManager.AppSettings["InsertFiu"];
                string numeFiu = ConfigurationManager.AppSettings["FiuTable"];
                this.statAdaptor.InsertCommand = new SqlCommand(insertFiu, conn);
                int nrColumns = ds2.Tables[numeFiu].Columns.Count;

                for (i= 0; i < nrColumns - 2; i++)
                {
                    //pentru nume si Student(Da/Nu)
                    value = "@param" + (i + 1).ToString();
                    statAdaptor.InsertCommand.Parameters.Add(value, SqlDbType.VarChar).Value = textBoxList[i].Text;
                }

                //pentru cheia straina, deci pentru id-ul de la casierie
                value = value = "@param" + (i + 1).ToString();
                statAdaptor.InsertCommand.Parameters.Add(value, SqlDbType.VarChar).Value = idA;
                statAdaptor.InsertCommand.ExecuteNonQuery();
                MessageBox.Show("Adaugarea s-a executat!");
                conn.Close();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                string deleteFiu = ConfigurationManager.AppSettings["deleteFiu"];
                this.statAdaptor.DeleteCommand = new SqlCommand(deleteFiu, conn);
                DataGridViewRow row = this.stateGridView.SelectedRows[0];
                String ID_S = row.Cells[0].Value.ToString();
                string id = ConfigurationManager.AppSettings["id"];
                this.statAdaptor.DeleteCommand.Parameters.Add(id, SqlDbType.VarChar).Value = Int16.Parse(ID_S);
                conn.Open();
                statAdaptor.DeleteCommand.ExecuteNonQuery();
                ds2.Clear();
                string numeFiu = ConfigurationManager.AppSettings["FiuTable"];
                statAdaptor.Fill(ds2, numeFiu);
                //statAdaptor.Fill(ds2, "state");
                
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }

}
